function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function

	try{

// distance correctly throws InvalidType if one of the parameters is not an array (20%)	
	if( Array.isArray(first) == false || Array.isArray(second) == false)  throw ("InvalidType")
	
// distance correctly differentiates between numbers and strings representing numbers (20%)
	for(i=0; i< first.length;i++)
		first[i]=parseInt(first[i],10)

	for(i=0; i< second.length;i++)
		second[i]=parseInt(second[i],10)		

// distance correctly ignores duplicate elements (20%)	
	first = Array.from(new Set(first))
	second = Array.from(new Set (second))

// distance correctly returns the result for 2 arrays (20%)
	var count = 0
	for(i=0; i < first.length; i++) {
		for(j=0; j < second.length ; j++)
			if(first[i]== second[j])
				count++
	}

	var distance=(first.length+second.length)-2*count

	console.log(distance)
	
	}
	catch(err){

		console.log(err)
	}


}

 distance( ['1','2',1,5],[2,3,4,4])

module.exports.distance = distance





